package com.uabc.entidad;

import com.uabc.entidad.CorteTurno;
import com.uabc.entidad.Credencial;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-12-10T16:35:17")
@StaticMetamodel(Visitas.class)
public class Visitas_ { 

    public static volatile SingularAttribute<Visitas, Integer> horas;
    public static volatile SingularAttribute<Visitas, String> tipoPago;
    public static volatile SingularAttribute<Visitas, Integer> idvisitas;
    public static volatile SingularAttribute<Visitas, String> turno;
    public static volatile ListAttribute<Visitas, CorteTurno> corteTurnoList;
    public static volatile SingularAttribute<Visitas, Credencial> idcredencial;

}