/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uabc.entidad;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author user
 */
@Entity
@Table(name = "seguro")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Seguro.findAll", query = "SELECT s FROM Seguro s")
    , @NamedQuery(name = "Seguro.findByIdseguro", query = "SELECT s FROM Seguro s WHERE s.idseguro = :idseguro")
    , @NamedQuery(name = "Seguro.findByNombre", query = "SELECT s FROM Seguro s WHERE s.nombre = :nombre")
    , @NamedQuery(name = "Seguro.findByApellidoPaterno", query = "SELECT s FROM Seguro s WHERE s.apellidoPaterno = :apellidoPaterno")
    , @NamedQuery(name = "Seguro.findByApellidoMaterno", query = "SELECT s FROM Seguro s WHERE s.apellidoMaterno = :apellidoMaterno")
    , @NamedQuery(name = "Seguro.findByNumeroSeguro", query = "SELECT s FROM Seguro s WHERE s.numeroSeguro = :numeroSeguro")
    , @NamedQuery(name = "Seguro.findByEgresado", query = "SELECT s FROM Seguro s WHERE s.egresado = :egresado")
    , @NamedQuery(name = "Seguro.findByFechaNacimiento", query = "SELECT s FROM Seguro s WHERE s.fechaNacimiento = :fechaNacimiento")
    , @NamedQuery(name = "Seguro.findByCorreo", query = "SELECT s FROM Seguro s WHERE s.correo = :correo")
    , @NamedQuery(name = "Seguro.findByFolio", query = "SELECT s FROM Seguro s WHERE s.folio = :folio")})
public class Seguro implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idseguro")
    private Integer idseguro;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "apellido_paterno")
    private String apellidoPaterno;
    @Basic(optional = false)
    @Column(name = "apellido_materno")
    private String apellidoMaterno;
    @Basic(optional = false)
    @Column(name = "numero_seguro")
    private String numeroSeguro;
    @Basic(optional = false)
    @Column(name = "egresado")
    private String egresado;
    @Basic(optional = false)
    @Column(name = "fecha_nacimiento")
    @Temporal(TemporalType.DATE)
    private Date fechaNacimiento;
    @Basic(optional = false)
    @Column(name = "correo")
    private String correo;
    @Basic(optional = false)
    @Column(name = "folio")
    private String folio;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idseguro")
    private List<Credencial> credencialList;

    public Seguro() {
    }

    public Seguro(Integer idseguro) {
        this.idseguro = idseguro;
    }

    public Seguro(Integer idseguro, String nombre, String apellidoPaterno, String apellidoMaterno, String numeroSeguro, String egresado, Date fechaNacimiento, String correo, String folio) {
        this.idseguro = idseguro;
        this.nombre = nombre;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.numeroSeguro = numeroSeguro;
        this.egresado = egresado;
        this.fechaNacimiento = fechaNacimiento;
        this.correo = correo;
        this.folio = folio;
    }

    public Integer getIdseguro() {
        return idseguro;
    }

    public void setIdseguro(Integer idseguro) {
        this.idseguro = idseguro;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getNumeroSeguro() {
        return numeroSeguro;
    }

    public void setNumeroSeguro(String numeroSeguro) {
        this.numeroSeguro = numeroSeguro;
    }

    public String getEgresado() {
        return egresado;
    }

    public void setEgresado(String egresado) {
        this.egresado = egresado;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    @XmlTransient
    public List<Credencial> getCredencialList() {
        return credencialList;
    }

    public void setCredencialList(List<Credencial> credencialList) {
        this.credencialList = credencialList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idseguro != null ? idseguro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Seguro)) {
            return false;
        }
        Seguro other = (Seguro) object;
        if ((this.idseguro == null && other.idseguro != null) || (this.idseguro != null && !this.idseguro.equals(other.idseguro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uabc.entidad.Seguro[ idseguro=" + idseguro + " ]";
    }
    
}
