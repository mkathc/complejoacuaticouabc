 package com.uabc.vista.Helper;

import complejo.entidad.Curso;
import com.uabc.negocio.integracion.serviceFacadeLocator;
import java.io.Serializable;
import java.util.List;

/** 
 * @author Axel Valenzuela - Complejo Acuatico 2018
 */
public class cursoBeanHelper implements Serializable{
    /**
     * Metodo para guardar un Cursos 
     * @param cursos
     */
    public void agregarNuevoCursos(Curso cursos){ 
        serviceFacadeLocator.getInstanceCursosFacade().saveCursos(cursos);
    }
    /**
     * Metodo para buscar un Cursos por ID
     * @param id 
     * @return  
     */
    public Curso consultarCursosByID(int id){ 
     return serviceFacadeLocator.getInstanceCursosFacade().findByID(id);
    }
    
    /**
     * Metodo para buscar todos los Cursos 
     * @param id 
     * @return  
     */
    public List<Curso> consultarTodosLosCursos(){ 
     return serviceFacadeLocator.getInstanceCursosFacade().findAll();
    }
    /**
     * Metodo para eliminar un Cursos mediante un id 
     * @param id
     */
    public void eliminarCurso(int id) {
        serviceFacadeLocator.getInstanceCursosFacade().eliminarCurso(id);
    }
    /**
     * Metodo para modificar un  Cursos 
     * @param cursos
     */
    public void updateCurso(Curso cursos){
        serviceFacadeLocator.getInstanceCursosFacade().updateCursos(cursos);
    }    
}
