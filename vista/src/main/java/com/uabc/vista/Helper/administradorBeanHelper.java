package com.uabc.vista.Helper;  
import complejo.entidad.Administrador;
import com.uabc.negocio.integracion.serviceFacadeLocator;
import java.io.Serializable;
import java.util.List;

/** 
 * @author Axel Valenzuela - Complejo Acuatico 2018
 */
public class administradorBeanHelper implements Serializable{
    /**
     * Metodo para guardar un administrador 
     * @param administrador
     */
    public void agregarNuevoAdministrador(Administrador administrador){ 
        serviceFacadeLocator.getInstanceAdministradorFacade().saveAdministrador(administrador);
    }
    /**
     * Metodo para buscar un administrador por ID
     * @param id 
     * @return  
     */
    public Administrador consultarAdministradorByID(int id){ 
     return serviceFacadeLocator.getInstanceAdministradorFacade().findByID(id);
    }
    
    /**
     * Metodo para buscar todos los administrador 
     * @return  
     */
    public List<Administrador> consultarTodosLosAdministrador(){ 
     return serviceFacadeLocator.getInstanceAdministradorFacade().findAll();
    }
    /**
     * Metodo para eliminar un administrador mediante un id 
     * @param id
     */
    public void eliminarAdministrador(int id) {
        serviceFacadeLocator.getInstanceAdministradorFacade().eliminarAdministrador(id);
    }
    /**
     * Metodo para modificar un  administrador 
     * @param administrador
     */
    public void updateAdministrador(Administrador administrador){
        serviceFacadeLocator.getInstanceAdministradorFacade().updateAdministrador(administrador);
    }    
}
